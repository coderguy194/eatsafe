//
//  Constants.h
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FlightData.h"
#define MAX_HEIGHT 250
@interface Constants : NSObject
enum GAME_OBJECT_TYPE{FOOD,HARMFUL,CLEANER};
enum FOOD_TYPE{COOKIE,CAKE,CHEESE,APPLE,TOAST};
enum HARMFUL_TYPE{MUD,GREEN_GERM,VILLAIN_GERM};

+(CGPoint)pointFromValue:(NSValue*)val;
+(int)pointForFoodType:(enum FOOD_TYPE)f_type;
+(UIImage*)imageForFoodType:(enum FOOD_TYPE)f_type;
+(UIImage*)imageForHarmType:(enum HARMFUL_TYPE)h_type;
+(FlightData*)dataForXSpan:(double)d AndTimeSpan:(double)t;
+(NSMutableArray*)flightPathFrom:(CGPoint)point ForDiff:(double)d AndFlightData:(FlightData*)data;
@end
