//
//  FlightData.h
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlightData : NSObject
-(id)initWithTheta:(double)t AndVelocity:(double)v;
@property double theta;
@property double velocity;

@end
