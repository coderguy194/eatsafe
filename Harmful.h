//
//  Harmful.h
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameObject.h"

@interface Harmful : GameObject
{
    enum HARMFUL_TYPE harmful_type;
    int point;
}

@end
