//
//  Food.m
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Food.h"

@implementation Food
-(id)initWithType:(enum FOOD_TYPE)f_type
{
    self=[super init];
    type=FOOD;
    food_type=f_type;
    point=[Constants pointForFoodType:food_type];
    image=[Constants imageForFoodType:food_type];
    handWashConsumeRate=-10;
    return  self;
}
@end
