////
//  Constants.m
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Constants.h"

@implementation Constants
+(int)pointForFoodType:(enum FOOD_TYPE)f_type
{
    int val;
    switch (f_type) {
        case APPLE:
            val=10;
            break;
        case CAKE:
            val=20;
            break;
        case CHEESE:
            val=5;
            break;
        case COOKIE:
            val=50;
            break;
        case TOAST:
            val=30;
            break;
    }
    return val;
}
+(UIImage*)imageForFoodType:(enum FOOD_TYPE)f_type
{
    NSString *img_nm=[[NSString alloc] init];
    switch (f_type) {
        case APPLE:
            img_nm=@"food2.png";
            break;
        case CHEESE:
            img_nm=@"food5.png";
            break;
        case CAKE:
            img_nm=@"food3.png";
            break;
        case TOAST:
            img_nm=@"food4.png";
            break;
        case COOKIE:
            img_nm=@"food1.png";
            break;
    }
    UIImage *image=[UIImage imageNamed:img_nm];
    return image;
}
+(UIImage*)imageForHarmType:(enum HARMFUL_TYPE)h_type
{
    NSString *img_nm=[[NSString alloc] init];
    switch (h_type) {
        case GREEN_GERM:
            img_nm=@"germ1.png";
            break;
        case VILLAIN_GERM:
            img_nm=@"germ2.png";
            break;
        case MUD:
            img_nm=@"mud.png";
            break;
    }
    UIImage *image=[UIImage imageNamed:img_nm];
    return image;
}

+(FlightData*)dataForXSpan:(double)d AndTimeSpan:(double)t
{ 
    //double theta = ((arc4random() % (4*MAX_HEIGHT)/d) + 0);
    double theta=20*M_PI/180;
    double velocity=(d/(t*cos(theta)));
    return [[FlightData alloc] initWithTheta:theta AndVelocity:velocity];
}
+(CGPoint)pointFromValue:(NSValue*)val
{
    return [val CGPointValue];
}
+(NSMutableArray*)flightPathFrom:(CGPoint)point ForDiff:(double)d AndFlightData:(FlightData*)data
{
    NSMutableArray *array=[[NSMutableArray alloc] init];
    for(int i=1;i<=8;i++)
    {
        if(i!=2 && i!=7)
        {
            double newX=point.x+i*d/8;
            CGPoint newPoint=CGPointMake(newX, newX*tan(data.theta)+point.y-9.8*newX*newX/(2*data.velocity*data.velocity*cos(data.theta)*cos(data.theta)));
            NSLog(@"%dth: x=%f y=%f",i,newPoint.x,newPoint.y);
            [array addObject:[NSValue valueWithCGPoint:newPoint]];
        }
    }
    return array;
}

@end
