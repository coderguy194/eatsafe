//
//  ViewController.h
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeInterval.h"

@interface ViewController : UIViewController
{

    IBOutlet UIImageView *titleImage;
    IBOutlet UIButton *startButton;
    IBOutlet UIButton *endButton;
    CGPoint previousStartButtonCenter;
    CGPoint previousEndButtonCenter;
}
+(void)setUpGameBackground:(NSString*)fileName ForView:(UIView *)view;

@end
