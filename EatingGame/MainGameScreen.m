//
//  MainGameScreen.m
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainGameScreen.h"

@implementation MainGameScreen

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)movingAlongPathAView:(UIView*)view WithDuration:(double)time
{
    // Set up path movement
    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    pathAnimation.calculationMode=kCAAnimationPaced;
    pathAnimation.fillMode = kCAFillModeForwards;
    pathAnimation.removedOnCompletion = NO;
    pathAnimation.rotationMode = @"auto";
    //Your image frame.origin from where the animation need to get start
    CGPoint viewOrigin = view.center;
    FlightData *data=[Constants dataForXSpan:50 AndTimeSpan:time];
    NSLog(@"I was ok before flight path.");
    NSMutableArray *flightPath=[Constants flightPathFrom:viewOrigin ForDiff:50 AndFlightData:data];
    NSLog(@"I am ok after flight path.flight path contains %d items",[flightPath count]);
    //
    CGMutablePathRef curvedPath = CGPathCreateMutable();
    CGPathMoveToPoint(curvedPath, NULL, viewOrigin.x, viewOrigin.y);
    CGPathAddCurveToPoint(curvedPath, NULL, [Constants pointFromValue:[flightPath objectAtIndex:0]].x, 340-[Constants pointFromValue:[flightPath objectAtIndex:0]].y, [Constants pointFromValue:[flightPath objectAtIndex:1]].x, 340-[Constants pointFromValue:[flightPath objectAtIndex:1]].y, [Constants pointFromValue:[flightPath objectAtIndex:2]].x, 340-[Constants pointFromValue:[flightPath objectAtIndex:2]].y);
    CGPathAddCurveToPoint(curvedPath, NULL, [Constants pointFromValue:[flightPath objectAtIndex:3]].x,[Constants pointFromValue:[flightPath objectAtIndex:3]].y, [Constants pointFromValue:[flightPath objectAtIndex:4]].x,[Constants pointFromValue:[flightPath objectAtIndex:4]].y, [Constants pointFromValue:[flightPath objectAtIndex:5]].x,[Constants pointFromValue:[flightPath objectAtIndex:5]].y+100);
    pathAnimation.path = curvedPath;
    pathAnimation.duration=time;
    CGPathRelease(curvedPath);
    [view.layer addAnimation:pathAnimation forKey:@"curveAnimation"];
}

-(UIImageView*)createObject
{
    UIImage *image=[UIImage imageNamed:[NSString stringWithFormat:@"food%d.png",1]];
    UIImageView *object=[[UIImageView alloc] initWithImage:image];
    object.frame=CGRectMake(100,0, 32, 32);
    [self.view addSubview:object];
    return object;
    //[array addObject:object];
    /*
    for(int i=1;i<=5;i++)
    {
    }*/
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

- (void)viewDidLoad
{
    [super viewDidLoad];
    array=[NSMutableArray array];
    [ViewController setUpGameBackground:@"game_bg.png" ForView:self.view];
    [self createObject];
    [self movingAlongPathAView:[self createObject]WithDuration:2];
        
}


- (void)viewDidUnload
{
    soapLevelIndicator = nil;
    scoreLable = nil;
    handImage = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortrait && interfaceOrientation !=UIInterfaceOrientationPortraitUpsideDown && interfaceOrientation !=UIInterfaceOrientationLandscapeLeft);
}

@end
