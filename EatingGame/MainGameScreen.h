//
//  MainGameScreen.h
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "FlightData.h"
#import "Constants.h"
#import "TimedObject.h"
@interface MainGameScreen : UIViewController
{
    
    IBOutlet UILabel *soapLevelIndicator;
    IBOutlet UILabel *scoreLable;
    IBOutlet UIImageView *handImage;
    
    NSMutableArray *array;
    
    
}

@end
