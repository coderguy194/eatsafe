//
//  ViewController.m
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}
#pragma mark-helper methods
- (void)moveTitleLeftAnimationWithin:(TimeInterval*)duration {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:duration.interval];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(titleAnimationDidStop:finished:context:)];
    
    CGPoint prevCenter=titleImage.center;
    titleImage.center=CGPointMake(160, prevCenter.y);
    
    [UIView commitAnimations];
}
-(void)letButtonsWithin:(float)durationInSecond
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:durationInSecond];
    [UIView setAnimationDelegate:self];
    
    startButton.center=previousStartButtonCenter;
    endButton.center=previousEndButtonCenter;
    
    [UIView commitAnimations];
}
//CALLED ONCE THE TITLE IS MOVED LEFT.
- (void)titleAnimationDidStop:(NSString*)animationID finished:(BOOL)finished context:(void *)context 
{
    //Title moved left,now lets see the buttons to come in.
    [self letButtonsWithin:0.3];
}
//STATIC METHOD FOR SETTING THE COMMON GAME BACKGROUND
+(void)setUpGameBackground:(NSString*)fileName ForView:(UIView*)view
{
    //programmatically set image for the paper view    
    UIColor *bgImg=[[UIColor alloc]initWithPatternImage:[UIImage imageNamed:fileName]];
    view.backgroundColor=bgImg;

}
-(void)popOutTitleAnimation
{
    [UIView animateWithDuration:0.3/2.5 animations:^{
        titleImage.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1,1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/3 animations:^{
            titleImage.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/3 animations:^{
                titleImage.transform = CGAffineTransformIdentity;                            
            }];
            
        }];
    }];
}

/*
 SET UPS INITIAL POSITIONS FOR BUTTONS.
 */
-(void)setUpInitialPositions
{
    previousStartButtonCenter=startButton.center;
    previousEndButtonCenter=endButton.center;
    startButton.center=CGPointMake(557,previousStartButtonCenter.y);
    endButton.center=CGPointMake(557, previousEndButtonCenter.y);
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [ViewController setUpGameBackground:@"game_bg.png" ForView:self.view];
    [self setUpInitialPositions];
    [self popOutTitleAnimation];
    [self performSelector:@selector(moveTitleLeftAnimationWithin:) withObject: [[TimeInterval alloc] initWithTime:0.3]afterDelay:1];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortrait && interfaceOrientation !=UIInterfaceOrientationPortraitUpsideDown && interfaceOrientation !=UIInterfaceOrientationLandscapeLeft);
}

@end
