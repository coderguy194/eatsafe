//
//  TimeInterval.m
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TimeInterval.h"

@implementation TimeInterval
@synthesize interval;
-(id)initWithTime:(double)time
{
    self=[super init];
    interval=time;
    return self;
}
@end
