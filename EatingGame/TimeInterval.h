//
//  TimeInterval.h
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeInterval : NSObject
@property double interval;
-(id)initWithTime:(double)time;
@end
