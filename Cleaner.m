//
//  Cleaner.m
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Cleaner.h"

@implementation Cleaner
-(id)init
{
    self=[super init];
    type=CLEANER;
    handWashConsumeRate=50;
    return self;
}
@end
