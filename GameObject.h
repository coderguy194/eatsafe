//
//  GameObject.h
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
@interface GameObject : NSObject
{
    enum GAME_OBJECT_TYPE type;
    UIImage *image;
    int handWashConsumeRate;
    int priority;
    
}

@end
