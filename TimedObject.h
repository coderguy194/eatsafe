//
//  TimedObject.h
//  EatingGame
//
//  Created by Sankalpa Ghose on 12/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimedObject : NSObject
@property(strong,nonatomic) UIView *view;
@property double initialX;
@property double initialY;
@property double d;
@property double theta;
@property double v;
@property double nowIncrementValue;
@property double incrementRate;
@end
