//
//  FlightData.m
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FlightData.h"

@implementation FlightData
@synthesize velocity,theta;
-(id)initWithTheta:(double)t AndVelocity:(double)v
{
    self=[super init];
    theta=t;
    velocity=v;
    return self;
}
@end
