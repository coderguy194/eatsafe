//
//  Food.h
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameObject.h"
#import "Constants.h"

@interface Food : GameObject
{
    enum FOOD_TYPE food_type;
    int point;
}

@end
