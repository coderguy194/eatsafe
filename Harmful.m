//
//  Harmful.m
//  EatingGame
//
//  Created by Sankalpa Ghose on 11/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Harmful.h"

@implementation Harmful
-(id)initWithType:(enum HARMFUL_TYPE)h_type
{
    self=[super init];
    type=HARMFUL;
    harmful_type=h_type;
    point=-10;
    image=[Constants imageForHarmType:h_type];
    handWashConsumeRate=-100;
    return  self;
}

@end
